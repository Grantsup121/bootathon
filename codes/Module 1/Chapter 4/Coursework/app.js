var a1 = document.getElementById("a1");
var a2 = document.getElementById('a2');
var a3 = document.getElementById('a3');
var b1 = document.getElementById('b1');
var b2 = document.getElementById('b2');
var b3 = document.getElementById('b3');
var m1 = document.getElementById('m1');
var m2 = document.getElementById('m2');
var m3 = document.getElementById('m3');
var d1 = document.getElementById('d1');
var d2 = document.getElementById('d2');
var d3 = document.getElementById('d3');
function add() {
    if ((Number(a1.value) && Number(a2.value)) || a1.value == '0' || a2.value == '0') {
        var c = parseFloat(a1.value) + parseFloat(a2.value);
        a3.value = c.toString();
    }
    else {
        alert("Enter a valid Number");
    }
}
function sub() {
    if ((Number(b1.value) && Number(b2.value)) || b1.value == '0' || b2.value == '0') {
        var num1 = parseFloat(b1.value);
        var num2 = parseFloat(b2.value);
        var c = num1 - num2;
        b3.value = c.toString();
    }
    else {
        alert("Enter a valid Number");
    }
}
function pro() {
    if ((Number(m1.value) && Number(m2.value)) || m1.value == '0' || m2.value == '0') {
        var c = parseFloat(m1.value) * parseFloat(m2.value);
        m3.value = c.toString();
    }
    else {
        alert("Enter a valid Number");
    }
}
function divide() {
    if ((Number(d1.value) && Number(d2.value)) || d1.value == '0' || d2.value == '0') {
        var num1 = parseFloat(d1.value);
        var num2 = parseFloat(d2.value);
        if (num2 != 0) {
            var c = num1 / num2;
        }
        else {
            alert("Zero Error : Divide by Zero Not Possible");
        }
        d3.value = c.toString();
    }
    else {
        alert("Enter a valid Number");
    }
}
//# sourceMappingURL=app.js.map