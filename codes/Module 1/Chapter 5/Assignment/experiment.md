# Caesar Cipher: Encryption

### Aim

To understand the encryption of a message using Caesar Cipher in a secure message exchange.

### Theory

**Cryptography Algorithms:** cryptographic algorithms are used to transfer electronic data over the internet so that no third-party is able to read the data. The strength

of the code is judged according to four parameters confidentiality, Integrity, Non – repudiation and authentication.

![Image 1](theoryimg_1.png)

**Caesar Cipher Cryptography:** The Caesar Cipher, also known as a shift cipher, is one of the oldest and simplest forms of encrypting a message. It is a type of substitution cipher where each letter in the original message (which in cryptography is called the laintext) is replaced with a letter corresponding to a certain number of letters shifted up or down in the alphabet. As shown in fig1 given below for each letter of the alphabet, you would take its position in the alphabet, say 3 for the letter 'A', and shift it by the key number. If we had a key of +3, that 'A' would be shifted down to an 'D' - and that same process would be applied to every letter in the plaintext. In this way, a message that initially was quite readable, ends up in a form that cannot be understood at a simple glance.

![Image 2](theoryimg_2.jpg)

**The Caesar Cipher can be expressed in a more mathematical form as follows:** In plain terms, this means that the encryption of a letter x is equal to a shift of x + n, where n is the number of letters shifted. The result of the process is then taken under modulo division, essentially meaning that if a letter is shifted past the end of the alphabet, it wraps around to the beginning.

### Procedure

During encrypting a message an input is considered as plain text. On plain text we apply the key

using a specific key length ( K = 0,1,2, 3….n). Key value must be any positive natural number

Based on the value of the key, each letter of the plain text is shifted to the Kth position using the

formula E= (X+ n) mod 26 to get the cipher text. X is plaintext letter and n is the value of key,

modulus is taken for value 26 because there are total 26 letter in English alphabet. Cipher text

is considered as the text generated from plain text after the shifting the plain text letters Kth

position. These shifted letters are unable to interpret in form a meaningful message and due to

this it helps during the secure message exchange between the sender and receiver.

**How to Perform Simulation:**

1.  Enter the plain text that you want to encrypt in the input text.
2.  Enter a valid key in the key box.
3.  Think and solve manually and enter the result in guess cipher text box.
4.  Hit submit.
5.  Simulator will display your answer is right or wrong.

*   If your answer is correct you can practice using reset.
*   If your answer is wrong, you can use hint and re-enter the guessed cipher text or can see the  
    solution.

7.  By practicing on simulator you can easily learn Caesar Cipher algorithm.